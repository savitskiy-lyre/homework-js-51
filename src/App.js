import Lottery from "./Components/Lottery/Lottery";

const App = () => (
  <div className="App">
     <Lottery/>
  </div>
);

export default App;
