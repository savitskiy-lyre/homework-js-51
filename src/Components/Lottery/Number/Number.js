import React from 'react';
import styles from './Number.module.css';

const Number = (props) => {
   return (
     <div className={styles.wrapper}>
        <span>{props.number}</span>
     </div>
   );
};

export default Number;