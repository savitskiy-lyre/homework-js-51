import React, {useState} from 'react';
import styles from './Lottery.module.css';
import Number from "./Number/Number";

const Lottery = () => {
   const randomUniqueArr = () => {
      const uniqueArr = new Set();
      do {
         uniqueArr.add(Math.floor(Math.random() * 32) + 5);
      } while (uniqueArr.size < 5)
      return Array.from(uniqueArr).sort((a,b)=>{
         return a-b;
      });
   };
   const [state, setState] = useState(randomUniqueArr());

   const reRandomNumbers = () => {
      setState(randomUniqueArr);
   };
   return (
     <div>
        <div className={styles.wrapperBtn}>
           <button onClick={reRandomNumbers}>New numbers</button>
        </div>
        <div className={styles.wrapperNumbers}>
           <Number number={state[0]}/>
           <Number number={state[1]}/>
           <Number number={state[2]}/>
           <Number number={state[3]}/>
           <Number number={state[4]}/>
        </div>
     </div>
   );
};

export default Lottery;